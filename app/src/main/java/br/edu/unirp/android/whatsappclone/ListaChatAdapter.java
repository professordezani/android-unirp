package br.edu.unirp.android.whatsappclone;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.URL;
import java.util.List;

public class ListaChatAdapter  extends BaseAdapter {

    private final List<Mensagem> lista;
    private final Activity activity;

    public ListaChatAdapter(List<Mensagem> lista, Activity activity) {
        this.lista = lista;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = this.activity.getLayoutInflater()
                .inflate(R.layout.item_chat, parent, false);

        TextView txtNome = (TextView)view.findViewById(R.id.txtNome);
        TextView txtTexto = (TextView)view.findViewById(R.id.txtTexto);
        ImageView imagem = (ImageView)view.findViewById(R.id.imagem);

        Mensagem mensagem = lista.get(position);

        txtNome.setText(mensagem.getDisplayName());
        txtTexto.setText(mensagem.getTexto());

        if(mensagem.getImagem() != null) {
            downloadImagem(imagem, mensagem.getImagem());
        }

        return view;
    }

    private void downloadImagem(final ImageView imagem, final String param) {
        new Thread() {
            @Override
            public void run() {
                try {
                    URL url = new URL(param);
                    final Bitmap bitmap = BitmapFactory
                            .decodeStream(url.openConnection().getInputStream());

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imagem.setImageBitmap(bitmap);
                        }
                    });
                }
                catch(Exception ex) {

                }
            }
        }.start();
    }
}
